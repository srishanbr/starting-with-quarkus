package org.agoncal.quarkus.starting;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class BookRepository {

    @ConfigProperty(name = "books.genre", defaultValue = "Sci-fi")
    String genre;
    public List<Book> getAllBooks() {
        return List.of(
                new Book(1, "title1", "author1", 2000, genre),
                new Book(2, "title2", "author2", 2001, genre),
                new Book(3, "title3", "author3", 2002, genre),
                new Book(4, "title4", "author4", 2003, genre)
        );
    }

    public int countAllBooks() {
        return getAllBooks().size();
    }

    public Optional<Book> getBook(int id) {
        return getAllBooks().stream().filter(book -> book.id == id).findFirst();
    }

}